package com.example.marcok.kalkulator;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.marcok.kalkulator.databinding.ActivityMainBinding;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final char ADDITION = '+';
    private static final char SUBTRACTION = '-';
    private static final char MULTIPLICATION = '*';
    private static final char DIVISION = '/';
    private static final char SIN = 's';
    private static final char COS = 'c';
    private static final char TAN = 't';

    private char CURRENT_ACTION;
    Button button7,button8,button9,button4,button5,button6,button1,button2,button3,button0,dotButton,resetButton,sinButton,cosButton,tanButton,equalsButton,plusOperand,minusOperand,multiOperand,divOperand,shareButton;
    private double valueOne = Double.NaN;
    private double valueTwo;
    private double valueHehe;
    TextView typer,displayer;

    private ArrayList<Double> simpanan = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        button7 = (Button) findViewById(R.id.number7);
        button8 = (Button) findViewById(R.id.number8);
        button9 = (Button) findViewById(R.id.number9);
        button4 = (Button) findViewById(R.id.number4);
        button5 = (Button) findViewById(R.id.number5);
        button6 = (Button) findViewById(R.id.number6);
        button1 = (Button) findViewById(R.id.number1);
        button2 = (Button) findViewById(R.id.number2);
        button3 = (Button) findViewById(R.id.number3);
        button0 = (Button) findViewById(R.id.number0);
        sinButton = (Button) findViewById(R.id.sinbutton);
        cosButton = (Button) findViewById(R.id.cosbutton);
        tanButton = (Button) findViewById(R.id.tanbutton);
        dotButton = (Button) findViewById(R.id.dotbutton);
        equalsButton = (Button) findViewById(R.id.equalsbutton);
        plusOperand = (Button) findViewById(R.id.first_button);
        minusOperand = (Button) findViewById(R.id.second_button);
        multiOperand = (Button) findViewById(R.id.third_button);
        divOperand = (Button) findViewById(R.id.forth_button);
        resetButton = (Button) findViewById(R.id.reset_button);
        shareButton = (Button) findViewById(R.id.sharebutton);

        displayer = (TextView) findViewById(R.id.displayer);
        typer = (TextView) findViewById(R.id.typer);

        final DecimalFormat decimalFormat = new DecimalFormat("#.#########");

        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typer.setText(typer.getText() + "7");
            }
        });
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typer.setText(typer.getText() + "8");
            }
        });
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typer.setText(typer.getText() + "9");
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typer.setText(typer.getText() + "4");
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typer.setText(typer.getText() + "5");
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typer.setText(typer.getText() + "6");
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typer.setText(typer.getText() + "1");
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typer.setText(typer.getText() + "2");
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typer.setText(typer.getText() + "3");
            }
        });
        dotButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typer.setText(typer.getText() + ".");
            }
        });
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                typer.setText(typer.getText() + "0");
            }
        });
        plusOperand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                computeCalculation();
                CURRENT_ACTION = ADDITION;
                displayer.setText(decimalFormat.format(valueOne)+"+");
                typer.setText(null);
            }
        });
        minusOperand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                computeCalculation();
                CURRENT_ACTION = SUBTRACTION;
                displayer.setText(decimalFormat.format(valueOne)+"-");
                typer.setText(null);
            }
        });
        multiOperand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                computeCalculation();
                CURRENT_ACTION = MULTIPLICATION;
                displayer.setText(decimalFormat.format(valueOne)+"x");
                typer.setText(null);
            }
        });
        divOperand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                computeCalculation();
                CURRENT_ACTION = DIVISION;
                displayer.setText(decimalFormat.format(valueOne)+":");
                typer.setText(null);
            }
        });
        equalsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                computeCalculation();
                displayer.setText(displayer.getText().toString() + decimalFormat.format(valueTwo)+"="+decimalFormat.format(valueOne));
                //valueOne = Double.NaN;
                CURRENT_ACTION = '0';
            }
        });
        sinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CURRENT_ACTION = SIN;
                computeCalculation();
                displayer.setText("sin("+typer.getText()+"="+decimalFormat.format(valueOne));
                typer.setText(null);
            }
        });
        cosButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                computeCalculation();
                CURRENT_ACTION = COS;
                displayer.setText("cos("+typer.getText().toString()+"="+decimalFormat.format(valueOne));
                typer.setText(null);
            }
        });
        tanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                computeCalculation();
                CURRENT_ACTION = TAN;
                displayer.setText("tan("+typer.getText().toString()+"="+decimalFormat.format(valueOne));
                typer.setText(null);
            }
        });
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayer.setText(null);
                typer.setText(null);
                valueOne = Double.NaN;
            }
        });

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                String shareBody = "Gunakan kalkulator ini! Lalu bagikan kepada teman kalian!";
                String shareSub = "Lalu berikan review!";
                shareIntent.putExtra(Intent.EXTRA_SUBJECT,shareSub);
                shareIntent.putExtra(Intent.EXTRA_TEXT,shareBody);
                startActivity(Intent.createChooser(shareIntent,"Ayo dipilih!"));
            }
        });

}

    private void computeCalculation() {
        if (!Double.isNaN(valueOne)) {
            valueTwo = Double.parseDouble(typer.getText().toString());
            typer.setText(null);

            if (CURRENT_ACTION == ADDITION) {
                valueOne = this.valueOne + valueTwo;
            } else if (CURRENT_ACTION == SUBTRACTION) {
                valueOne = this.valueOne - valueTwo;
            } else if (CURRENT_ACTION == MULTIPLICATION) {
                valueOne = this.valueOne * valueTwo;
            } else if (CURRENT_ACTION == DIVISION) {
                valueOne = this.valueOne / valueTwo;
            } if (CURRENT_ACTION == SIN) {
                //valueTwo = 0;
                valueOne = Math.sin(Math.toRadians(this.valueOne));
            }
            if (CURRENT_ACTION == COS) {
                valueOne = Math.cos(Math.toRadians(this.valueTwo));
            }
            if (CURRENT_ACTION == TAN)
                valueOne = Math.tan(Math.toRadians(this.valueTwo));
            simpanan.add(0, valueOne);
        }
         else {
            try {
                valueOne = Double.parseDouble(typer.getText().toString());
                //simpanan.add(0,valueOne);
            } catch (Exception e) {
            }
        }
    }
    //Reference from http://www.androidauthority.com/build-a-calculator-app-721910/
}
